package co.uk.magmo.broccolitickets;

import static co.uk.magmo.broccolitickets.Utilities.Messenger.playerMessage;

import co.uk.magmo.broccolitickets.Commands.Create;
import co.uk.magmo.broccolitickets.Commands.List;
import co.uk.magmo.broccolitickets.Commands.Pick;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDispatcher implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commmandlabel, String[] args) {
        Player player = (Player) sender;

        if (args.length == 0) {
            playerMessage(player, "This command is not recognised", true);
            return true;
        }

        switch (args[0].toLowerCase()) {
            case "c":
            case "create":
            case "new": {
                Create.newTicket(sender, args);
                return true;
            }

            case "l":
            case "list": {
                List.listTickets(sender);
                return true;
            }

            case "p":
            case "pick": {
                Pick.pickTicket(sender, args);
            }

            default:
                playerMessage(player, "This command is not recognised", true);
                return true;
        }
    }
}
